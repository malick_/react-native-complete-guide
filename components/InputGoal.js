import React, { useState } from "react";
import { StyleSheet, View, TextInput, Button } from "react-native";

function InputGoal(props) {
  const [enteredGoal, setEnteredGoal] = useState("");
  const goalInputHandler = (inputText) => {
    setEnteredGoal(inputText);
    // console.log(enteredGoal);
  };

  return (
    <View style={styles.inputContainer}>
      <TextInput
        placeholder="Course goal"
        style={styles.input}
        onChangeText={goalInputHandler}
        value={enteredGoal}
      />
      <Button title="ADD" onPress={props.inputHandler.bind(this, enteredGoal)} />
    </View>
  );
}

const styles = StyleSheet.create({
  inputContainer: { flexDirection: "row", justifyContent: "space-between", alignItems: "center" },
  input: { borderColor: "black", borderWidth: 1, padding: 10, width: "80%" },
});

export default InputGoal;
