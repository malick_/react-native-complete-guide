import React from "react";
import { FlatList, StyleSheet, View, Text, TouchableOpacity } from "react-native";

function Goals(props) {
  return (
    <FlatList
      data={props.data}
      keyExtractor={(item, index) => item.id}
      renderItem={(itemData) => (
        <TouchableOpacity onPress={props.onDelete.bind(this, itemData.item.id)}>
          <View style={styles.goal}>
            <Text>{itemData.item.value}</Text>
          </View>
        </TouchableOpacity>
      )}
    />
  );
}

const styles = StyleSheet.create({
  goal: {
    color: "black",
    marginVertical: 5,
    padding: 10,
    backgroundColor: "lightgreen",
  },
});

export default Goals;
