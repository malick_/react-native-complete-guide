import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput, Button, ScrollView, FlatList } from "react-native";
import InputGoal from "./components/InputGoal";
import Goals from "./components/Goals";

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);

  const addGoalHandler = (enteredGoal) => {
    setCourseGoals((currentGoals) => [
      ...currentGoals,
      { id: Math.random().toString(), value: enteredGoal },
    ]);
  };

  const removeGoalHandler = (id) => {
    const currentGoals = courseGoals.filter((courseGoal) => courseGoal.id !== id);
    setCourseGoals(currentGoals);
  };
  return (
    <View style={styles.screen}>
      <InputGoal inputHandler={addGoalHandler} />
      <Goals data={courseGoals} onDelete={removeGoalHandler} />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: { padding: 50 },
  inputContainer: { flexDirection: "row", justifyContent: "space-between", alignItems: "center" },
  input: { borderColor: "black", borderWidth: 1, padding: 10, width: "80%" },
  goal: {
    color: "black",
    marginVertical: 5,
    padding: 10,
    backgroundColor: "lightgreen",
  },
});

// TODO: revisite flexbox styling concept
